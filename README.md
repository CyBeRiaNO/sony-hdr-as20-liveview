#Sony Action Cam HDR-AS20 live view
This is a project to access the Sony HDR-AS20 Action Cam Liveview via **Sony Camera Remote API** using python

**This project require python3**

### Live view window (Require pygame)
#### Install pygame
```
pip3 install pygame
```
#### Run live view window
Connect to camera WiFi, then run:
```
python3 liveview.py
```

### Live view browser streaming (Require flask)
#### Install flask
```
pip3 install flask
```
#### Run live view browser streaming
Connect to camera WiFi, then run:
```
python3 liveviewStreaming.py
```
Now open browser to [http://localhost:5000]