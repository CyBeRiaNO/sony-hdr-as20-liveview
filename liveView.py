#!/usr/bin/python

import http.client
import json
import struct
import pygame
import io

pygame.init()

display_width = 640
display_height = 360

screen = pygame.display.set_mode((display_width,display_height))

pygame.display.set_caption('Action Cam HDR-AS20 Live View - Sony Remote API')

print("Starting live view via API...")
conn = http.client.HTTPConnection("192.168.122.1", 10000, timeout=5)
params = {"method": "startLiveview","params": [],"id": 1,"version": "1.0"}
headers = {"Content-type": "application/json"}
conn.request("POST", "/sony/camera", json.dumps(params), headers)
res = conn.getresponse()
print(res.status, res.reason)
print(res.read())

print("Connecting to stream...")
conn = http.client.HTTPConnection("192.168.122.1", 60152, timeout=5)
conn.request("GET", "/liveview.JPG?%211234%21http%2dget%3a%2a%3aimage%2fjpeg%3a%2a%21%21%21%21%21")
res = conn.getresponse()
print(res.status, res.reason)

crashed = False
while not crashed and not res.closed:
  for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True

  commonHeaderLength = 1 + 1 + 2 + 4
  commonHeader = res.read(commonHeaderLength)

  #print(commonHeader);

  payloadHeaderLength = 4 + 3 + 1 + 4 + 1 + 115
  payloadHeader = res.read(payloadHeaderLength)

  #print(payloadHeader);

  jpegSize = struct.unpack('>i',b'\x00' + payloadHeader[4:7])[0]
  #print(jpegSize);

  paddingSize = ord(payloadHeader[7:8])

  jpegData = res.read(jpegSize)
  paddingData = res.read(paddingSize)

  stream = io.BytesIO(jpegData)

  frame = pygame.image.load(stream)

  screen.blit(frame,(0,0))

  pygame.display.flip()

print("Closing stream...")
conn.close()
print("Bye!")
pygame.quit()
quit()
