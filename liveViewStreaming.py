#!/usr/local/bin/python3

import http.client
import json
import struct
import pygame
import io

from flask import Flask, render_template, Response

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

def gen(res):
  while not res.closed:
    commonHeaderLength = 1 + 1 + 2 + 4
    commonHeader = res.read(commonHeaderLength)

    #print(commonHeader);

    payloadHeaderLength = 4 + 3 + 1 + 4 + 1 + 115
    payloadHeader = res.read(payloadHeaderLength)

    #print(payloadHeader);

    jpegSize = struct.unpack('>i',b'\x00' + payloadHeader[4:7])[0]
    #print(jpegSize);

    paddingSize = ord(payloadHeader[7:8])

    jpegData = res.read(jpegSize)
    paddingData = res.read(paddingSize)

    yield (b'--frame\r\n'
           b'Content-Type: image/jpeg\r\n\r\n' + jpegData + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
  print("Starting live view via API...")
  conn = http.client.HTTPConnection("192.168.122.1", 10000, timeout=5)
  params = {"method": "startLiveview","params": [],"id": 1,"version": "1.0"}
  headers = {"Content-type": "application/json"}
  conn.request("POST", "/sony/camera", json.dumps(params), headers)
  res = conn.getresponse()
  print(res.status, res.reason)
  print(res.read())

  print("Connecting to camera stream...")
  conn = http.client.HTTPConnection("192.168.122.1", 60152, timeout=5)
  conn.request("GET", "/liveview.JPG?%211234%21http%2dget%3a%2a%3aimage%2fjpeg%3a%2a%21%21%21%21%21")
  res = conn.getresponse()
  print(res.status, res.reason)
  print("Streaming to /video_feed")

  return Response(gen(res), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
